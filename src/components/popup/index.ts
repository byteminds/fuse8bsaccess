import './scss/index.scss';
import { render } from 'react-dom';
import { createElement } from 'react';
import { renderContainer } from 'general/js/render-container';
import { Popup } from './js/popup';

render(createElement(Popup, null, null), renderContainer);
