import React, { HTMLAttributes } from 'react';
import classnames from 'classnames';
import { useStore } from 'effector-react';
import { setName, userStore } from 'general/js/user-store';

export const NameInput = ({
    className,
    ...props
}: HTMLAttributes<HTMLInputElement>): JSX.Element => {
    const { name } = useStore(userStore);
    return (
        <input
            {...props}
            type="text"
            className={classnames(className, 'f8-input')}
            placeholder="Your name"
            defaultValue={name}
            onChange={(e) => setName(e.target.value)}
        />
    );
};
