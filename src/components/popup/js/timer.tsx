import React, { HTMLAttributes } from 'react';
import { useStore } from 'effector-react';
import { sessionStore } from 'general/js/session-store';
import classnames from 'classnames';

export const Timer = ({
    className,
    ...props
}: HTMLAttributes<HTMLDivElement>): JSX.Element | null => {
    const { timer } = useStore(sessionStore);
    if (!timer) return null;
    return (
        <div className={classnames(className, 'f8-timer')} {...props}>
            {timer}
        </div>
    );
};
