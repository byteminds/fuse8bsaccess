import React, { HTMLAttributes } from 'react';
import classnames from 'classnames';
import { logOut } from 'general/js/session-store';
import axios from 'axios';
import { logoutAccount } from 'general/js/endpoints';
import { useStore } from 'effector-react';
import { userStore } from 'general/js/user-store';

export const StopBtn = ({
    className,
    ...props
}: HTMLAttributes<HTMLButtonElement>): JSX.Element => {
    const { id } = useStore(userStore);
    const onClick = () => {
        axios
            .post(logoutAccount, {
                userId: id?.userId,
                accountId: id?.accountId,
            })
            .then(() => {
                logOut();
            });
    };
    return (
        <button
            className={classnames(className, 'f8-btn')}
            {...props}
            title="stop session"
            type="button"
            onClick={onClick}
        >
            stop
        </button>
    );
};
