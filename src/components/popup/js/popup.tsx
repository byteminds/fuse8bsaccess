import React, { useState } from 'react';
import classnames from 'classnames';
import { NameInput } from 'components/popup/js/name-input';
import { Timer } from 'components/popup/js/timer';
import { useStore } from 'effector-react';
import { sessionStore } from 'general/js/session-store';
import { StopBtn } from 'components/popup/js/stop-btn';
import { ProceedSession } from 'components/popup/js/proceed-session';

export const Popup = (): JSX.Element => {
    const { isLoggedIn, isLoading } = useStore(sessionStore);
    const [isActive, setActive] = useState(false);
    const [isOpened, setOpened] = useState(false);

    const onTrEnd = () => setOpened(isActive);
    return (
        <div
            className={classnames('popup', {
                'is-active': isActive,
                'is-opened': isOpened,
                'is-loading': isLoading,
            })}
            onTransitionEnd={onTrEnd}
        >
            <button className="popup__btn f8-btn" onClick={() => setActive(!isActive)}>
                {isLoggedIn ? <Timer /> : (isActive && 'close') || 'open'}
            </button>
            {isLoggedIn && (
                <div className="popup__right-side">
                    <StopBtn />
                </div>
            )}
            <div className="popup__content">
                <div className="popup__content-inner">
                    <NameInput />
                    <ProceedSession />
                </div>
            </div>
        </div>
    );
};
