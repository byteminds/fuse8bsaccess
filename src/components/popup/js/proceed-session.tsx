import React, { useState } from 'react';
import { useStore } from 'effector-react';
import { proceedSession, sessionStore } from 'general/js/session-store';
import axios, { AxiosResponse } from 'axios';
import { proceedAccount } from 'general/js/endpoints';
import { userStore } from 'general/js/user-store';

interface IProceedResponse {
    statusCode: number;
    responseDate: string;
}

export const ProceedSession = (): JSX.Element | null => {
    const [time, setTime] = useState('5');
    const [isLoading, setLoading] = useState(false);
    const { isLoggedIn } = useStore(sessionStore);
    const { id } = useStore(userStore);
    if (!isLoggedIn) return null;
    const onClick = () => {
        setLoading(true);
        axios
            .post(proceedAccount, { userId: id?.userId, accountId: id?.accountId })
            .then((data: AxiosResponse<IProceedResponse>) => {
                console.log(data);
                if (data.data.statusCode === 200) {
                    proceedSession(+time);
                }
            })
            .finally(() => {
                setLoading(false);
            });
    };
    return (
        <>
            <div className="f8-range">
                <div className="f8-range__spans">
                    {['00', '05', '10', '15', '20', '25', '30']
                        .join(' . ')
                        .split(' ')
                        .map((number) => (
                            <span>{number}</span>
                        ))}
                </div>
                <input
                    type="range"
                    min="0"
                    max="30"
                    className="input-lg f8-range__input"
                    id="f8-range"
                    value={time}
                    onChange={(e) => setTime(e.target.value)}
                    step="1"
                />
            </div>
            <button
                className="f8-btn"
                title="proceed session"
                type="button"
                disabled={isLoading}
                onClick={onClick}
            >
                Proceed session
            </button>
        </>
    );
};
