import axios, { AxiosResponse } from 'axios';
import { paramsToQuery } from 'general/js/params-to-query';
import { setID, userStore } from 'general/js/user-store';
import { bookAccount } from 'general/js/endpoints';
import { logIn } from 'general/js/session-store';

interface IServerResponse {
    statusCode: number;
    responseData: {
        name: string;
        accountId: string | number;
        password: string;
        userId: string;
        expirationTime: string;
    };
}

const form = document.querySelector<HTMLFormElement>('#signin_signup_form');
const submitBtn = form?.querySelector<HTMLButtonElement>('#user_submit');
const loginInput = form?.querySelector<HTMLInputElement>('#user_email_login');
const passInput = form?.querySelector<HTMLInputElement>('#user_password');

if (form && submitBtn && loginInput && passInput) {
    form.classList.add('f8-form');
    const customSubmit = document.createElement('button');
    customSubmit.setAttribute('type', 'button');
    customSubmit.className = submitBtn.className;
    customSubmit.innerHTML = 'Fuse8 sign in';
    const parent = submitBtn.parentElement as HTMLDivElement;
    parent.classList.add('f8-submit');
    customSubmit.classList.add('f8-submit__custom-btn');
    submitBtn.classList.add('f8-submit__native-btn');
    parent.appendChild(customSubmit);

    parent.parentElement?.insertAdjacentHTML(
        'beforebegin',
        `<div class="input-placeholder col-lg-6 col-center">
            <div class="input-wrapper f8-range">
                <div class="f8-range__spans">
                    ${['00', '05', '10', '15', '20', '25', '30']
                        .map((number) => `<span>${number}</span>`)
                        .join('.')}
                </div>
                <input type="range" min="0" max="30" class="input-lg f8-range__input" id="f8-range" value="5" step="1"/>
            </div>
        </div>`
    );
    const input = form.querySelector<HTMLInputElement>('#f8-range');
    customSubmit.addEventListener('click', () => {
        const { name } = userStore.getState();
        const time = input?.value ?? '5';
        form.classList.add('is-blocked');
        axios
            .get(`${bookAccount}?${paramsToQuery({ time, name })}`)
            .then(({ data, status }: AxiosResponse<IServerResponse>) => {
                if (data.statusCode === 200) {
                    const { userId, password, expirationTime, name, accountId } = data.responseData;
                    setID({ userId, accountId });
                    logIn({
                        credentials: { login: name, pass: password },
                        dieDate: new Date(expirationTime),
                    });
                    loginInput.value = name;
                    passInput.value = password;
                    submitBtn.click();
                } else if (status === 204) {
                    console.log('something went wrong in 200', data);
                }
            })
            .catch((e) => {
                console.log('something went wrong, in catch ', e);
            })
            .finally(() => {
                form.classList.remove('is-blocked');
            });
    });
} else {
    console.warn('sorry, seems this page is not a bs login');
}
