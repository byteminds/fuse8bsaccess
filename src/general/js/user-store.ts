import { createEvent, createStore } from 'effector';
import Cookie from 'js-cookie';

interface IBackIds {
    accountId: number | string;
    userId: string;
}

interface IUserStorage {
    name: string;
    id?: IBackIds;
}

const STORAGE_KEY = 'f8-user';

const isStorage = (x: any): x is IUserStorage => typeof x.name === 'string';

const getFromCookie = (): IUserStorage => {
    const storageString = Cookie.get(STORAGE_KEY);
    try {
        const result = JSON.parse(storageString || '{}');
        if (result && isStorage(result)) return result;
    } catch (e) {}
    return {
        name: '',
    };
};

const setStoreInCookie = (store: IUserStorage) => {
    const expires = new Date();
    expires.setFullYear(expires.getFullYear() + 5);
    Cookie.set(STORAGE_KEY, JSON.stringify(store), {
        domain: 'browserstack.com',
        secure: true,
        expires,
    });
};

export const setName = createEvent<string>();
export const setID = createEvent<IBackIds>();
export const removeId = createEvent();

export const userStore = createStore<IUserStorage>(getFromCookie())
    .on(setName, (state, name) => {
        const _state = {
            ...state,
            name,
        };
        setStoreInCookie(_state);
        return _state;
    })
    .on(setID, (state, id) => {
        const _state = {
            ...state,
            id,
        };
        setStoreInCookie(_state);
        return _state;
    })
    .on(removeId, (state) => {
        const _state = {
            ...state,
            id: undefined,
        };
        setStoreInCookie(_state);
        return _state;
    });
