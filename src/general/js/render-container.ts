import { unmountComponentAtNode } from 'react-dom';

const renderContainer = document.createElement('div');
document.body.appendChild(renderContainer);

const unmountModal = () => unmountComponentAtNode(renderContainer);

export { renderContainer, unmountModal };
