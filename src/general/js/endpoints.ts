const baseAddress = 'https://f8bsaccess.deletestaging.com';

export const bookAccount = `${baseAddress}/api/account/book`;
export const proceedAccount = `${baseAddress}/api/account/extendBooking`;
export const logoutAccount = `${baseAddress}/api/account/freeAccount`;
export const listOfFree = `${baseAddress}/api/account/getFreeAccounts`;
