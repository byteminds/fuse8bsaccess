const pages = [
    {
        name: 'dashb',
        urls: ['https://live.browserstack.com/*'],
        entrys: ['app'],
    },
    {
        name: 'all',
        urls: ['https://www.browserstack.com/*'],
        entrys: ['app'],
    },
    {
        name: 'sign-in',
        urls: ['https://www.browserstack.com/users/sign_in'],
        entrys: ['sign-in'],
    },
];

module.exports = {
    pages,
};
