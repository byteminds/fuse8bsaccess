import { createEvent, createStore } from 'effector';
import Cookie from 'js-cookie';
import { removeId } from 'general/js/user-store';

const LOG_OUT_LINK = 'https://www.browserstack.com/users/sign_out';
const COOKIE_KEY = 'f8-session';

interface ICredentials {
    login: string;
    pass: string;
}

type IDateString = string;

interface ISession {
    credentials?: ICredentials;
    dieDate?: Date;
    timer?: string;
}

interface IStorageSpecific {
    isLoggedIn: boolean;
    isLoading: boolean;
}

interface CookieSession {
    credentials: ICredentials;
    date: IDateString;
}

type ISessionStorage = ISession & IStorageSpecific;

const isCookieSession = (s: any): s is CookieSession => {
    return !!s.credentials;
};

const getSessionFromCookie = (): ISession | undefined => {
    const sessionString = Cookie.get(COOKIE_KEY);
    if (!sessionString) return;
    const session = JSON.parse(sessionString);
    if (!isCookieSession(session)) return;
    return {
        credentials: session.credentials,
        dieDate: new Date(session.date as string),
    };
};

const setSessionToCookie = (session: ISession | ISessionStorage): void => {
    if (!session.dieDate) return;
    Cookie.set(
        COOKIE_KEY,
        JSON.stringify({
            credentials: session.credentials,
            date: session.dieDate.toUTCString(),
        }),
        {
            domain: 'browserstack.com',
            expires: session.dieDate,
            secure: true,
        }
    );
};

const removeCookie = () =>
    Cookie.remove(COOKIE_KEY, {
        domain: 'browserstack.com',
        secure: true,
    });

export const logIn = createEvent<ISession>();
export const proceedSession = createEvent<number>();
export const logOut = createEvent();

const initialSession = getSessionFromCookie();

const TIME_OPTIONS: Intl.DateTimeFormatOptions = {
    minute: '2-digit',
    second: '2-digit',
};

export const dateFormat = new Intl.DateTimeFormat('en-GB', TIME_OPTIONS).format;
const callTick = createEvent<string>();

const getDifference = (dieDate: Date): string | undefined => {
    const difference = +dieDate - +new Date();
    if (difference < 0) return;
    return dateFormat(difference);
};

const timeTick = (dieDate: Date): void => {
    if (!dieDate) return;
    setTimeout(() => {
        const difference = getDifference(dieDate);
        if (difference) {
            callTick(difference);
        } else {
            logOut();
        }
    }, 1000);
};

export const sessionStore = createStore<ISessionStorage>({
    credentials: initialSession?.credentials,
    dieDate: initialSession?.dieDate,
    isLoggedIn: !!initialSession,
    timer: initialSession?.dieDate && getDifference(initialSession?.dieDate),
    isLoading: false,
})
    .on(logOut, (state) => {
        removeId();
        removeCookie();
        window.location.href = LOG_OUT_LINK;
        return {
            ...state,
            credentials: undefined,
            dieDate: undefined,
            isLoggedIn: false,
            isLoading: true,
            timer: undefined,
        };
    })
    .on(logIn, (state, session) => {
        setSessionToCookie(session);
        timeTick(session.dieDate as Date);
        return {
            ...state,
            ...session,
            isLoggedIn: true,
            isLoading: true,
            timer: getDifference(session.dieDate as Date),
        };
    })
    .on(proceedSession, (state, minutes) => {
        if (!state.dieDate) return state;
        console.log(state.dieDate);
        state.dieDate.setMinutes(state.dieDate.getMinutes() + minutes);
        console.log(state.dieDate);
        setSessionToCookie(state);
        return {
            ...state,
            timer: getDifference(state.dieDate),
        };
    })
    .on(callTick, (state, timer) => {
        timeTick(state.dieDate as Date);
        return {
            ...state,
            timer,
        };
    });

if (initialSession?.dieDate) timeTick(initialSession.dieDate);
