export const normalizeString = (str: string) =>
    // eslint-disable-next-line no-empty-character-class
    encodeURIComponent(str.toLowerCase().replace(/- !?[]{}/g, '-'));
