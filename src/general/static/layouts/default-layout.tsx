import React, { ReactNode } from 'react';
import classnames from 'classnames';

export const DefaultLayout = ({
    children,
    title,
    chunks,
}: IDefaultLayout): JSX.Element => (
    <html className="html" lang="en">
        <head>
            <title>{title}</title>
            <meta name="viewport" content="width=device-width" />
            <meta charSet="utf-8" />
        </head>
        <body className={classnames('body')}>
            {children}
            <div id="scripts" data-scripts={chunks?.join('|')} />
        </body>
    </html>
);

export interface IDefaultLayout {
    title: string;
    children: ReactNode;
    chunks?: string[];
}
