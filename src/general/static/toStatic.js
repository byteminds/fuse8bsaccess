import ReactDOMServer from 'react-dom/server.js';
import React from 'react';

export const toStatic = (app) => {
    return `<!DOCTYPE html> ${ReactDOMServer.renderToStaticMarkup(app())}`;
};
