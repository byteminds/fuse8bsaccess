import React, { ReactNode } from 'react';
import { DefaultLayout } from 'general/static/layouts/default-layout';
import { toStatic } from 'general/static/toStatic.js';

const MyTest = (): ReactNode => (
    <DefaultLayout
        title="test page"
    >
        hello world
    </DefaultLayout>
);

export default () => toStatic(MyTest);
