# Fuse 8 Browser stack access

This is util, for convenient using shared accounts from [confluence](https://fuse8ru.atlassian.net/wiki/spaces/fuse8/pages/2030927873) 

##Installation

### UI part
1. clone repository
2. install node modules via "npm install" or "yarn"
3. choose needed build
   1. for development mode call "npm run dev" or "yarn dev"
   2. for production(with code minification) mode call "npm run prod" or "yarn prod"
