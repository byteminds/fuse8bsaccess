const _ = require('lodash');
const { pages } = require('../src/general/js/pages.js');

const _seed = {};
const configureManifest = (_entrypoints) => ({
    seed: _seed,
    filter: ({ name }) => !name.endsWith('.map') || !name.endsWith('.txt'),

    generate: (seed, files, entrypoints) => {
        const manifestFiles = files.reduce((manifest, { name, path }) => {
            manifest[name] = path;
            return manifest;
        }, seed);

        const entrypointFiles = {};

        for (const [key, value] of Object.entries(entrypoints)) {
            let _value = value;

            if (key !== 'app') {
                _value = value.filter((fileName) => !fileName.includes('runtime'));
            }

            const js = _value
                .filter((fileName) => fileName.endsWith('.js'))
                .map((file) => `/${file}`);
            const mjs = _value
                .filter((fileName) => fileName.endsWith('.mjs'))
                .map((file) => `/${file}`);
            const css = _value
                .filter((fileName) => fileName.endsWith('.css'))
                .map((file) => `/${file}`);

            _.set(entrypointFiles, `${key}.js`, js);
            _.set(entrypointFiles, `${key}.mjs`, mjs);
            _.set(entrypointFiles, `${key}.css`, css);

            // eslint-disable-next-line no-restricted-syntax
            for (const [fileName, filePath] of Object.entries(manifestFiles)) {
                const fileRegexp = /(?<name>[\w][\w-]*).(?<hash>[a-zA-Z0-9]*).(?<ext>[\w]{2,4}$)/;

                if (fileName.match(fileRegexp)) {
                    const groups = fileName.match(fileRegexp).groups;

                    if (groups && groups.name === key && fileName.endsWith('.svg')) {
                        _.set(entrypointFiles, `${key}.svg`, [filePath.replace('./', '')]);
                    }
                }
            }
        }

        const resultEntryPoints = _.merge(_entrypoints, entrypointFiles);
        const content_scripts = pages
            .map((page) => {
                const pageEntrys = Object.entries(resultEntryPoints)
                    .filter(([key]) => page.entrys.includes(key))
                    .map(([key, value]) => value);
                if (pageEntrys && pageEntrys.length) {
                    return {
                        matches: page.urls,
                        css: _.uniq(...pageEntrys.map((entr) => entr.css)),
                        js: _.uniq(...pageEntrys.map((entr) => entr.js)),
                    };
                }
                return null;
            })
            .filter((x) => !!x);

        return {
            //entrypoints: resultEntryPoints,
            name: 'Fuse8BSAccess',
            manifest_version: 2,
            content_scripts,
            icons: { 128: 'icon_128.png' },
            version: '1.0',
            browser_action: {
                default_icon: 'icon_128.png',
                //default_popup: 'index.html',
            },
            permissions: ['activeTab'],
        };
    },
});

module.exports = {
    configureManifest,
};
