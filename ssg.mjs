import webpack from 'webpack';
import { cpus } from 'os';
import { readdir, writeFile, rm, mkdir, mkdtemp, readFile } from 'fs/promises';
import path, { join, resolve } from 'path';
import { tmpdir } from 'os';
import babelConfig from './babel.config.js';
import { parse } from 'node-html-parser';
import beautify from 'beautify';

const getFiles = async (dir) => {
    try {
        const dirents = await readdir(dir, { withFileTypes: true });
        const files = await Promise.all(
            dirents.map((dirent) => {
                const res = resolve(dir, dirent.name);
                return dirent.isDirectory() ? getFiles(res) : res;
            })
        );
        return Array.prototype.concat(...files);
    } catch (e) {
        return [];
    }
};

const run = (compiler) =>
    new Promise((res) => {
        compiler.run((err, stats) => {
            if (err) {
                console.log('failed', err);
                res();
                return;
            }
            res();
        });
    });

const srcStatic = './src/general/static/';
const buildFolder = './dist';

const normalizePath = (path) => path.replace('/dist/', '');

const insertScripts = async (htmlString) => {
    const root = parse(htmlString, { comment: true });
    const scriptsDiv = root.querySelector('#scripts');
    const head = root.querySelector('head');
    const body = root.querySelector('body');
    const pageEntryPoints = scriptsDiv?.getAttribute('data-scripts')?.split('|') || [];
    const manifestString = await readFile(`${buildFolder}/manifest.json`, { encoding: 'utf8' });
    if (manifestString) {
        const manifestOptions = JSON.parse(manifestString);
        if (!pageEntryPoints.includes('app')) pageEntryPoints.push('app');
        if (scriptsDiv) {
            await Promise.all(
                pageEntryPoints.map(async (name) => {
                    const entry = manifestOptions.entrypoints[name];
                    if (!entry) return;
                    if (entry.css) {
                        entry.css.forEach((css) => {
                            head.insertAdjacentHTML(
                                'beforeend',
                                `<link rel="preload" href="${css}" as="style">`
                            );
                            head.insertAdjacentHTML(
                                'beforeend',
                                `<link rel="stylesheet" href="${css}">`
                            );
                        });
                    }
                    const js = entry.js || entry.mjs;
                    if (js) {
                        js.forEach((js) => {
                            head.insertAdjacentHTML(
                                'beforeend',
                                `<link rel="preload" href="${js}" as="script">`
                            );
                            scriptsDiv.insertAdjacentHTML(
                                'beforeend',
                                `<script defer src='${js}'></script>`
                            );
                        });
                    }
                    if (entry.svg) {
                        return Promise.all(
                            entry.svg.map((svg) => readFile(`.${svg}`, { encoding: 'utf8' }))
                        ).then((files) => {
                            files.forEach((svgString) => {
                                body.insertAdjacentHTML('afterbegin', svgString);
                            });
                        });
                    }
                })
            );
        }
        scriptsDiv.replaceWith(scriptsDiv.innerHTML);
        return root.toString();
    }
    console.log("styles and scripts wasn't added to static");
    return htmlString;
};

const main = async () => {
    const tempDir = await mkdtemp(join(tmpdir(), 'react-ssg-'));
    const files = await getFiles(`${srcStatic}/pages`);
    const entry = {};
    files.forEach((entryPath) => {
        entry[
            path.basename(entryPath, path.extname(entryPath))
        ] = `${srcStatic}pages/${path.basename(entryPath)}`;
    });
    try {
        const serverCompiler = webpack({
            module: {
                rules: [
                    {
                        test: /\.(jsx)$/,
                        exclude: /node_modules/,
                        use: [
                            {
                                loader: 'babel-loader',
                                options: babelConfig,
                            },
                        ],
                    },
                    {
                        test: /\.(tsx?)$/,
                        exclude: [/node_modules(?!(\/|\\)@deleteagency)/],
                        use: [
                            {
                                loader: 'babel-loader',
                                options: babelConfig,
                            },
                            {
                                loader: 'thread-loader',
                                options: {
                                    // there should be 1 cpu for the fork-ts-checker-webpack-plugin
                                    workers: cpus().length - 1,
                                    poolTimeout: 500, // set this to Infinity in watch mode - see https://github.com/webpack-contrib/thread-loader
                                },
                            },
                            {
                                loader: 'ts-loader',
                                options: {
                                    happyPackMode: true, // for thread-loader
                                },
                            },
                        ],
                    },
                ],
            },
            target: 'node',
            mode: 'development',
            node: {
                __dirname: false,
            },
            entry,
            resolve: {
                extensions: ['.js', '.jsx', '.ts', '.tsx'],
                modules: ['node_modules', './', './src'],
            },
            output: {
                path: tempDir,
                filename: '[name].js',
                libraryTarget: 'commonjs',
            },
        });
        await run(serverCompiler);
        const _generatedFiles = await getFiles(tempDir);
        try {
            await mkdir('./dist');
        } catch (e) {}
        if (_generatedFiles.length) {
            await Promise.all(
                _generatedFiles.map(async (filePath) => {
                    const url = `file:/\//${filePath.split('\\').join('/')}`;
                    const res = await import(url);
                    if (!res?.default?.default || !typeof res?.default?.default === 'function')
                        return;
                    const finalHtml = await insertScripts(res?.default?.default());
                    await writeFile(
                        path.join(
                            buildFolder,
                            `${path.basename(filePath, path.extname(filePath))}.html`
                        ),
                        beautify(finalHtml, { format: 'html' })
                    );
                })
            );
        }
        await rm(tempDir, { recursive: true, force: true });
        console.log('Static html has been built successfully');
    } catch (e) {
        console.log('Something went wrong in building static html: \n\\', e);
    }
};

main();
