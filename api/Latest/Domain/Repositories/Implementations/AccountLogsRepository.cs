﻿using Domain.Data;
using Domain.Entities;

namespace Domain.Repositories.Implementations
{
    public class AccountLogsRepository : GenericRepository<AccountLog>, IAccountLogsRepository
    {
        public AccountLogsRepository(DomainContext context) : base(context)
        {
        }
    }
}