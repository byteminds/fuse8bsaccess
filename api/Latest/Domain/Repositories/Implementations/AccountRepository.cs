﻿using System.Collections.Generic;
using System.Linq;
using Domain.Data;
using Domain.Entities;

namespace Domain.Repositories.Implementations
{
    public class AccountRepository : GenericRepository<Account>, IAccountRepository
    {
        private DomainContext _domainContext;
        public AccountRepository(DomainContext context) : base(context)
        {
            _domainContext = context;
        }
    }
}