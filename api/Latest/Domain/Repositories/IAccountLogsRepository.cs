﻿using Domain.Entities;

namespace Domain.Repositories
{
    public interface IAccountLogsRepository : IGenericRepository<AccountLog>
    {
        
    }
}