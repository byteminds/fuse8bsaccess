﻿using System.Collections.Generic;
using Domain.Entities;

namespace Domain.Repositories
{
    public interface IAccountRepository : IGenericRepository<Account>
    {
    }
}