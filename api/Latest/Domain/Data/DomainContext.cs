﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Data
{
    public sealed class DomainContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountLog> AccountLogs { get; set; }

        public DomainContext(DbContextOptions<DomainContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Account>(ConfigureAccount);
            builder.Entity<AccountLog>(ConfigureAccountLogs);
        }
        private void ConfigureAccount(EntityTypeBuilder<Account> builder)
        {
            builder.ToTable("Accounts");
            builder.HasKey(x => x.Id);
        }

        private void ConfigureAccountLogs(EntityTypeBuilder<AccountLog> builder)
        {
            builder.ToTable("AccountLogs");
            builder.HasKey(x => x.Id);
        }
    }
}