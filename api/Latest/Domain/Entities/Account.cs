﻿using System.Collections;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class Account : BaseEntity
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public ICollection<AccountLog> Logs { get; set; }

        public Account()
        {
            Logs = new HashSet<AccountLog>();
        }
    }
}