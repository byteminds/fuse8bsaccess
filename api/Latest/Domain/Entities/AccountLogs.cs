﻿using System;

namespace Domain.Entities
{
    public class AccountLog : BaseEntity
    {
        public int AccountId { get; set; }
        public string UserName { get; set; }
        public string Userid { get; set; }
        public DateTime ExpirationTime { get; set; }
    }
}