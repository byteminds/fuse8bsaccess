﻿using BusinessFacade.Models;

namespace SystemFacade.Extensions
{
    public static class AccountExtensions
    {
        public static bool IsEmpty(this AccountDto account)
        {
            return account == null || account.Name.IsEmpty() || account.Password.IsEmpty();
        }
    }
}