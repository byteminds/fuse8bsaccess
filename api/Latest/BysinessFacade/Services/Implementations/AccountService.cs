﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using BusinessFacade.Models;
using Domain.Entities;
using Domain.Repositories;
using AccountDto = BusinessFacade.Models.AccountDto;

namespace BusinessFacade.Services.Implementations
{
    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IAccountLogsRepository _accountLogsRepository;

        private const int MinutesInDay = 1440; 

        public AccountService(IAccountRepository accountRepository, IAccountLogsRepository accountLogsRepository)
        {
            _accountRepository = accountRepository;
            _accountLogsRepository = accountLogsRepository;
        }

        public async Task<AccountDto> Book(int rentalPeriod, string name)
        {
            var accounts = await _accountRepository.GetWithIncludeAsync(x => x.Logs);

            var account = accounts.FirstOrDefault(x => x.Logs.Count == 0)
                          ?? accounts.FirstOrDefault(x =>
                              x.Logs.OrderByDescending(logs => logs.Id).FirstOrDefault()?.ExpirationTime <
                              DateTime.Now);

            if (account == null)
            {
                return new AccountDto();
            }

            var userId = Guid.NewGuid().ToString();
            var expirationTime = DateTime.Now.AddMinutes(rentalPeriod);

            var accountLog = new AccountLog
            {
                AccountId = account.Id,
                UserName = name,
                Userid = userId,
                ExpirationTime = expirationTime
            };

            await _accountLogsRepository.CreateAsync(accountLog);

            return new AccountDto
            {
                AccountId = account.Id,
                Name = account.Name,
                Password = account.Password,
                ExpirationTime = expirationTime,
                UserId = userId
            };
        }

        public async Task<string> ExtendBooking(BookingRequest data)
        {
            var (isSuccess, message) = await UpdateAccount(data);

            return isSuccess 
                ? "Account booking was extend" 
                : message;
        }

        public async Task<string> FreeAccount(BookingRequest data)
        {
            data.Time = - MinutesInDay;
            var (isSuccess, message) = await UpdateAccount(data);

            return isSuccess
                ? "Account was free"
                : message;
        }

        public async Task<IEnumerable<string>> GetFreeAccounts()
        {
            var accounts = await _accountRepository.GetWithIncludeAsync(x => x.Logs);
            var result = new List<string>();

            foreach (var account in accounts)
            {
                var lastLog = account.Logs.OrderByDescending(x => x.Id).FirstOrDefault();
                if (lastLog == null || lastLog.ExpirationTime < DateTime.Now)
                {
                    result.Add(account.Name);
                }
            }

            return result;
        }

        private async Task<(bool, string)> UpdateAccount(BookingRequest data)
        {
            var lastAccountActivityLog = await GetLastAccountLog(data.AccountId);

            if (lastAccountActivityLog == null)
            {
                return (false, "Account is never used");
            }

            if (!VerifyAccount(data.UserId, lastAccountActivityLog.Userid))
            {
                return (false, "UserId is not correct");
            }

            var log = new AccountLog
            {
                Userid = lastAccountActivityLog.Userid,
                AccountId = lastAccountActivityLog.AccountId,
                UserName = lastAccountActivityLog.UserName,
                ExpirationTime = lastAccountActivityLog.ExpirationTime.AddMinutes(data.Time)
            };
            
            await _accountLogsRepository.CreateAsync(log);

            return (true, "Account was update");
        }

        private async Task<AccountLog> GetLastAccountLog(int accountId)
        {
            var accounts = await _accountRepository.GetWithIncludeAsync(account => account.Id == accountId, x => x.Logs);
            return accounts.FirstOrDefault()?.Logs.OrderByDescending(x => x.Id).FirstOrDefault();
        }

        private bool VerifyAccount(string requestUserId, string logUserId)
        {
            return string.Equals(requestUserId, logUserId, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}