﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessFacade.Models;

namespace BusinessFacade.Services
{
    public interface IAccountService
    {
        Task<AccountDto> Book(int rentalPeriod, string name);
        Task<string> ExtendBooking(BookingRequest data);
        Task<string> FreeAccount(BookingRequest data);
        Task<IEnumerable<string>> GetFreeAccounts();
    }
}