﻿namespace BusinessFacade.Models
{
    public class BookingRequest
    {
        public int AccountId { get; set; }
        public string UserId { get; set; }
        public int Time { get; set; }
    }
}