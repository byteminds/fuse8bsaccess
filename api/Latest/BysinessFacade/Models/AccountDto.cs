﻿using System;

namespace BusinessFacade.Models
{
    public class AccountDto
    {
        public int AccountId { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string UserId { get; set; }
        public DateTime ExpirationTime { get; set; }
    }
}