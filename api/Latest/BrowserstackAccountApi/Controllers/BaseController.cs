﻿using System.Net;
using BrowserstackAccountApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace BrowserstackAccountApi.Controllers
{
    public class BaseController : Controller
    {
        protected JsonResult ReturnJson(object data, HttpStatusCode code)
        {
            return new JsonResult(new ResponseDto
            {
                ResponseData = data,
                StatusCode = code
            });
        }

        protected JsonResult Ok(object data)
        {
            return ReturnJson(data, HttpStatusCode.OK);
        }

        protected JsonResult Error(object data)
        {
            return ReturnJson(data, HttpStatusCode.InternalServerError);
        }

        protected JsonResult NoContent(object data)
        {
            return ReturnJson(data, HttpStatusCode.NoContent);
        }
    }
}