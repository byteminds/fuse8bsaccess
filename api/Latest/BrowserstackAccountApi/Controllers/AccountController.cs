﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using BusinessFacade.Services;
using SystemFacade.Extensions;
using BrowserstackAccountApi.Models;
using BusinessFacade.Models;
using Microsoft.AspNetCore.Mvc;

namespace BrowserstackAccountApi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[Action]")]
    public class AccountController : BaseController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public async Task<IActionResult> Book(int time, string name)
        {
            AccountDto account;

            try
            {
                account = await _accountService.Book(time, name);
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }

            return account.IsEmpty() 
                ? NoContent("No free accounts. Try later.") 
                : Ok(account);
        }

        [HttpPost]
        public async Task<IActionResult> ExtendBooking([FromBody] BookingRequest input)
        {
            try
            {
              return Ok(await _accountService.ExtendBooking(input));
            }
            catch(Exception ex)
            {
                return Error(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> FreeAccount([FromBody] BookingRequest input)
        {
            try
            {
                return Ok(await _accountService.FreeAccount(input));
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }

        }

        public async Task<IActionResult> GetFreeAccounts()
        {
            IEnumerable<string> result;
            try
            {
                result = await _accountService.GetFreeAccounts();
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }

            return !result.Any()
                ? NoContent("No free accounts. Try later.")
                : Ok(result);
        }
    }
}