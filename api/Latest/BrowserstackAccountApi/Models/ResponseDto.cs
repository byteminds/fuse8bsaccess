﻿using System.Net;

namespace BrowserstackAccountApi.Models
{
    public class ResponseDto
    {
        public HttpStatusCode StatusCode { get; set; }
        public object ResponseData { get; set; }
    }
}