import fs from'fs';
import path from 'path';
import { exec } from 'child_process';
import debounce from 'lodash/debounce.js';

const root = 'src';
const timeout = 300;

const callback = (event, filename) => {
  if (!filename) {
    return;
  }

  const extname = path.extname(filename);

  if (extname === '.tsx') {
    console.log(`${filename} file Changed`);
    exec('npm run build:static');
  }
}

fs.watch(root, { recursive: true }, debounce(callback, timeout));